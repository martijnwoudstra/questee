package martijn.questee.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

/**
 * Minecraft
 * BlockQuestee
 * Created by Martijn on 9-10-2015.
 */
public class BlockQuestee extends Block {

    public BlockQuestee(){

        super(Material.rock);
    }
}
