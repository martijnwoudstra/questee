package martijn.questee.blocks;

import martijn.questee.lib.Names;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Minecraft
 * Blocks
 * Created by Martijn on 9-10-2015.
 */
public class Blocks {

    public static final BlockQuestee testBlock = new TestBlock();

    public static void registerBlocks() {

        GameRegistry.registerBlock(testBlock, Names.TESTBLOCK_NAME);
    }
}
