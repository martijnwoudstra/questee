package martijn.questee.blocks;

import martijn.questee.lib.Names;
import net.minecraft.creativetab.CreativeTabs;

/**
 * Minecraft
 * TestBlock
 * Created by Martijn on 9-10-2015.
 */
public class TestBlock extends BlockQuestee {

    public TestBlock() {

        super();
        this.setUnlocalizedName(Names.TESTBLOCK_NAME);
        this.setCreativeTab(CreativeTabs.tabCombat);
    }
}
