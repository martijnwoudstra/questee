package martijn.questee.items;

import martijn.questee.lib.Names;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * Minecraft
 * TestItem
 * Created by Martijn on 9-10-2015.
 */
public class TestItem extends ItemQuestee {

    public TestItem() {

        this.setUnlocalizedName(Names.TESTITEM_NAME);
        this.setCreativeTab(CreativeTabs.tabCombat);
    }
}
