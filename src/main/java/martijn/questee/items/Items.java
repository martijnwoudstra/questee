package martijn.questee.items;

import martijn.questee.lib.Names;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Minecraft
 * Items
 * Created by Martijn on 9-10-2015.
 */
public class Items {

    public static ItemQuestee testItem = new TestItem();

    public static void registerItems() {

        GameRegistry.registerItem(testItem, Names.TESTITEM_NAME);
    }
}
