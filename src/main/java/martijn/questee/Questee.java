package martijn.questee;

import martijn.questee.blocks.Blocks;
import martijn.questee.items.Items;
import martijn.questee.lib.References;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Minecraft
 * Questee
 * Created by Martijn on 9-10-2015.
 */

@Mod(modid = References.MODID, name = References.MODNAME, version = References.VERSION)
public class Questee {

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {

        registerBlocksAndItems();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }

    private void registerBlocksAndItems() {
        
        Blocks.registerBlocks();
        Items.registerItems();
    }
}
