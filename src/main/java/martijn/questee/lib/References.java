package martijn.questee.lib;

/**
 * Minecraft
 * References
 * Created by Martijn on 9-10-2015.
 */
public class References {

    public static final String MODID = "questee";
    public static final String MODNAME = "Questee";
    public static final String VERSION = "Pre Release";
}
